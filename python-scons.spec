Name:          scons
Version:       4.8.1
Release:       1
Summary:       An Open Source software construction tool
License:       MIT
URL:           http://www.scons.org
Source0:       https://github.com/SCons/scons/archive/%{version}/scons-%{version}.tar.gz
Source1:       https://scons.org/doc/%{version}/scons-doc-%{version}.tar.gz
BuildArch:     noarch
BuildRequires: make
BuildRequires: python3-pip  python3-hatchling python3-hatch-vcs

%description
SCons is an Open Source software construction tool—that is, a next-generation
build tool. Think of SCons as an improved, cross-platform substitute for the
classic Make utility with integrated functionality similar to autoconf/automake
and compiler caches such as ccache. In short, SCons is an easier, more reliable
and faster way to build software.

%package        doc
Summary:        An Open Source software construction tool
BuildArch:      noarch

%description    doc
Scons HTML documentation.

%package -n     python3-%{name}
Summary:        An Open Source software construction tool

BuildRequires:  python3-devel
BuildRequires:  python3-lxml
BuildRequires:  python3-wheel
BuildRequires:  python3-setuptools
BuildRequires:  lynx python3-psutil
Provides:       scons = 0:%{version}-%{release}
Provides:       scons-python3
Obsoletes:      scons < 0:5.0.3-3
%{?python_provide:%python_provide python3-%{name}}

%description -n python3-%{name}
SCons is an Open Source software construction tool—that is, a next-generation
build tool. Think of SCons as an improved, cross-platform substitute for the
classic Make utility with integrated functionality similar to autoconf/automake
and compiler caches such as ccache. In short, SCons is an easier, more reliable
and faster way to build software.

%prep
%autosetup -n %{name}-%{version} -N
%setup -n %{name}-%{version} -q -T -D -a 1

%build
%pyproject_build

%install
%pyproject_install

%check
%{__python3} runtest.py -P %{__python3} --passed --quit-on-failure SCons/BuilderTests.py

%files -n python3-%{name}
%doc CHANGES.txt RELEASE.*
%license LICENSE*
%{_bindir}/%{name}
%{_bindir}/%{name}{ign,-time,-configure-cache}
%{python3_sitelib}/SCons*

%files doc
%doc HTML PDF EPUB TEXT

%changelog
* Wed Nov 27 2024 xu_ping <707078654@qq.com> - 4.8.1-1
- Upgrade to version 4.8.1
  * Added more type annotations to internal routines.
  * Changed the message about scons -H to clarify it shows built-in options only.
  * Add JDK 21 LTS support
  * Fixed the Scanner examples in the User Guide to be runnable and added
some more explanation.
  * Add locking around creation of CacheDir config file. 

* Fri Dec 29 2023 liyanan <liyanan61@h-partners.com> - 4.5.2-1
- Upgrade to version 4.5.2

* Fri Jun 10 2022 SimpleUpdate Robot <tc@openeuler.org> - 4.3.0-1
- Upgrade to version 4.3.0

* Wed Dec 23 2020 lingsheng <lingsheng@huawei.com> - 3.1.1-4
- Keep spec name same as package

* Tue Oct 20 2020 zhanghua <zhanghua40@huawei.com> -  3.1.1-3
- remove python2 subpackage

* Thu Jun 4 2020 Captain Wei <captain.a.wei@gmail.com> - 3.1.1-2
- update package version

* Sat Nov 30 2019 zhouyihang <zhouyihang1@huawei.com> - 3.0.1-10
- Package init
